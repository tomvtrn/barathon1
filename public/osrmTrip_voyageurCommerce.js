L.Routing.OSRMTrip = L.Routing.OSRMv1.extend({
		_routeDone: function(response, inputWaypoints, options, callback, context) {
			var alts = [],
			    actualWaypoints,
			    i,
			    route;

			context = context || callback;
			if (response.code !== 'Ok') {
				callback.call(context, {
					status: response.code
				});
				return;
			}

			actualWaypoints = this._toWaypoints(inputWaypoints, response.waypoints);

			for (i = 0; i < response.trips.length; i++) {
				route = this._convertRoute(response.trips[i]);
				route.inputWaypoints = inputWaypoints;
				route.waypoints = actualWaypoints;
				route.properties = {isSimplified: !options || !options.geometryOnly || options.simplifyGeometry};
				alts.push(route);
			}

			this._saveHintData(response.waypoints, inputWaypoints);

			callback.call(context, null, alts);
		},
/*
    _toWaypoints: function(inputWaypoints, vias, permutation) {
        var wps = [],
            i, j;
        for (i = 0; i < permutation.length; i++) {
            for (j = 0; j < inputWaypoints.length; j++) {
                if (permutation[i] == j) {
                    wps.push(L.Routing.waypoint(L.latLng(vias[i]),
                                            inputWaypoints[j].name,
                                            inputWaypoints[j].options));
                }
            }
        }

        return wps;
    },
*/
		_toWaypoints: function(inputWaypoints, vias) {
			var wps = [],
			    i,
			    viaLoc;
			for (i = 0; i < vias.length; i++) {
				viaLoc = vias[i].location;
                    wps.push(L.Routing.waypoint(L.latLng(vias[i]),
                                            inputWaypoints[i].name,
                                            inputWaypoints[i].options));
			}

			return wps;
		},
		
    buildRouteUrl: function(waypoints, options) {
        var locs = [],
            wp,
            computeInstructions,
            locationKey,
            hint;

        for (var i = 0; i < waypoints.length; i++) {
                wp = waypoints[i];
                locationKey = this._locationKey(wp.latLng);
				
				newCoords = locationKey.split(',');
				
                locs.push(newCoords[1]+","+newCoords[0] + ";");

                hint = this._hints.locations[locationKey];
                if (hint) {
                        locs.push('hint=' + hint);
                }

                if (wp.options && wp.options.allowUTurn) {
                        locs.push('u=true');
                }
        }
		locs[locs.length-1] = locs[locs.length-1].substring(0, locs[locs.length-1].length - 1);

        computeInstructions = !(options && options.geometryOnly);

        return this.options.serviceUrl + '/foot/' +
                (options.z ? 'z=' + options.z + '&' : '') +
                locs.join('') + '?steps=true' + "&roundtrip=false&source=first&destination=last" +
                (this._hints.checksum !== undefined ? '&checksum=' + this._hints.checksum : '') +
                (options.fileformat ? '&output=' + options.fileformat : '') +
                (options.allowUTurns ? '&uturns=' + options.allowUTurns : '');
    },
});

L.Routing.osrmTrip = function(options) {
    return new L.Routing.OSRMTrip(options);
};