<?php

namespace App\Controller;

use App\Entity\Amis;
use App\Entity\User;
use App\Form\UserType;
use Doctrine\Common\Persistence\ObjectManager;
use ReCaptcha\RequestMethod\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Twig\Environment;
use App\Repository\UserRepository;

class UserController extends AbstractController
{

    // ----------------------- Partie Admin ------------------------
    // Affiche les utilisateurs
    /**
     * @Route("/admin/showUser", name="admin.showUser")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showUserAdmin(RegistryInterface $doctrine,Environment $twig){
        $user=$doctrine->getRepository(User::class)->findByRole('ROLE_USER');
        return new Response($twig->render('admin/user/showUser.html.twig', ['user' => $user]));
    }

    // Supprime un utilisateur

    /**
     * @Route("/admin/delete/user", name="admin.deleteUser", methods={"DELETE"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteUserAdmin(Request $request, Environment $twig, RegistryInterface $doctrine, FormFactoryInterface $formFactory)
    {
        $user=$doctrine->getRepository(User::class)->find($request->query->get('id'));
        $doctrine->getEntityManager()->remove($user);
        $doctrine->getEntityManager()->flush();
        return $this->redirectToRoute('admin.showUser');
    }

    //filtre des utilisateurs

    /**
     * @Route("/user/filtre", name="user.filtre")
     */
    public function filtreUser(RegistryInterface $doctrine,Environment $twig, Request $request){

        $user=$doctrine->getRepository(User::class)->filterUser($request->get("filtre"));
        $userC = $this->getUser();
        $amis = $doctrine->getRepository(Amis::class)->getAmis($userC);

        return new Response($twig->render('user/listeUser.html.twig', ['user' => $user, 'amis' => $amis]));
    }


    // modifier les données d'un utilisateur
    /**
     * @Route("/admin/editUser", name="admin.editUser", methods={"PUT"})
     */
    public function editUser(Request $request, RegistryInterface $doctrine, FormFactoryInterface $formFactory,UserPasswordEncoderInterface $encoder)
    {
        $user=$doctrine->getRepository(User::class)->find($request->query->get('id'));
        $form=$formFactory->createBuilder(UserType::class,$user,  array(
            //'action' => $this->generateUrl('target_route'),
            'method' => 'PUT',
        ))->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user,$user->getPassword());
            $user->setPassword($hash);
            $doctrine->getEntityManager()->flush();
            return $this->redirectToRoute('admin.showUser');
        }
        return $this->render('admin/user/editUser.html.twig',['form'=>$form->createView()]);
    }

    // ajouter un utilisateur
    /**
     * @Route("/admin/inscriptionUser", name="admin.inscriptionUser", methods={"POST"})
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder,AuthenticationUtils $authUtils, RegistryInterface $doctrine){
        $user = new User();
        $form= $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        $user->setRoles('ROLE_USER');
        if($form->isSubmitted() && $form->isValid()){
            $hash = $encoder->encodePassword($user,$user->getPassword());
            $user->setPassword($hash);
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('admin.showUser');
        }
        return $this->render('admin/user/inscription.html.twig', [
            'form'=>$form->createView()
        ]);
    }




    // ------------------  Partie User ------------------------
    // Affiche les utilisateurs pour pouvoirs les ajouter en amis
    /**
     * @Route("/User/list", name="user.userList")
     */
    public function showUser(RegistryInterface $doctrine,Environment $twig){
        $user=$doctrine->getRepository(User::class)->findByRole('ROLE_USER');
        $userC = $this->getUser();
        $amis = $doctrine->getRepository(Amis::class)->getAmis($userC);
      
        return new Response($twig->render('user/listeUser.html.twig', ['user' => $user, 'amis' => $amis]));
    }


}
