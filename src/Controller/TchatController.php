<?php



namespace App\Controller;

use App\Entity\Amis;
use App\Entity\Message;
use App\Entity\User;
use App\Form\CommentaireType;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TchatController extends AbstractController
{

    //---------------------------------- Envoyer un message a quelqu'un-----------------------------------------------------

    /**
     * @Route("/tchat/addMessage",name="amis.messages")
     */
    public function addMessage(Environment $twig, RegistryInterface $doctrine, Request $request, FormFactoryInterface $formFactory)
    {
        if ($request->request->get('contenue') != ""){
            $user1= $doctrine->getRepository(User::class)->find($request->request->get('id'));
            $user=$this->getUser();
            $message = new Message();
            $message->setUser1($user)
                ->setUser2($user1)
                ->setContenue($request->request->get('contenue'))
                ->setDate(new \DateTime('now'));
            $doctrine->getEntityManager()->persist($message);
            $doctrine->getEntityManager()->flush();
        }


        $messages= $doctrine->getRepository(Message::class)->findAll();
        $messagesArray = array(
            "contenue" => array(),
            "idUser1" => array(),
            "idUser2" => array(),
        );

        foreach ($messages as $messageSolo){
            array_push($messagesArray["contenue"], $messageSolo->getContenue());
            array_push($messagesArray["idUser1"], $messageSolo->getUser1()->getId());
            array_push($messagesArray["idUser2"], $messageSolo->getUser2()->getId());
        }

        $response = new Response(json_encode(array(
            'message' => "couscous",
            'messages' => $messagesArray
        )));
        $response->headers->set('Content-Type', 'application/json');
        //return $this->redirectToRoute('http://127.0.0.1:8000/tchat/'.$id);
        return $response;
    }

    /**
     * @Route("/tchat/{id}", name="amis.tchat")
     */
    public function showMessage(RegistryInterface $doctrine, Environment $twig, $id){
        $message= $doctrine->getRepository(Message::class)->findAll();
        $user1= $doctrine->getRepository(User::class)->find($id);
        return new Response($twig->render('tchat/tchat.html.twig', ['message' => $message, 'user1'=> $user1]));
    }

    // affiche les amis
    public function showAmis(Request $request, Environment $twig, RegistryInterface $doctrine)
    {
        $user = $this->getUser();
        $amis = $doctrine->getRepository(Amis::class)->getAmis($user);

        return $this->render('user/_showAmis.html.twig', [
            'amis' => $amis, 'user'=> $user
        ]);
    }


    // -------------------------------------- Supprimer un message -----------------------------------------
}



