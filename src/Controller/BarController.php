<?php

namespace App\Controller;

use App\Entity\Bar;
use App\Entity\BarLike;
use App\Entity\Commentaire;
use App\Entity\Notification;
use App\Form\BarType;
use App\Form\CommentaireType;
use App\Form\ModifBarType;
use App\Repository\BarLikeRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Twig\Environment;

class BarController extends AbstractController
{

    // ------------------- Partie Utilisateur -------------------------
    // ajouter un bar
    /**
     * @Route("/bar/add", name="bar.add")
     */
    public function ajouterBar(Request $request, Environment $twig, RegistryInterface $doctrine, FormFactoryInterface $formFactory){
        $form=$formFactory->createBuilder(BarType::class)->getForm();
        $form->handleRequest($request);
        $user=$this->getUser();
        if($form->isSubmitted() && $form->isValid()){
            $bar=$form->getData();
            $bar->setUsers($user);
            $doctrine->getEntityManager()->persist($bar);
            $doctrine->getEntityManager()->flush();

            return new Response($twig->render('accueil.html.twig'));
        }
        return $this->render('bar/formBar.html.twig', [
            'form' => $form->createView()
        ]);
    }
    // liste des bars
    /**
     * @Route("/showBar", name="bar.show")
     */
    public function showBar(Environment $twig, RegistryInterface $doctrine, Request $request){
        $bar=$doctrine->getRepository(Bar::class)->findAll();
        $commentaire = $doctrine->getRepository(Commentaire::class)->findAll();
        return new Response($twig->render('bar/showBar.html.twig', ['bar' => $bar, 'commentaire'=>$commentaire]));
    }

    /**
     * @Route("/bar/edit", name="bar.edit")
     */
    public function modifierBar(){

    }

    // liker ou disliker un bar

    /**
     * @Route("/showBar/like/{id}", name="bar.like")
     * @param ObjectManager $manager
     * @param Bar $bar
     * @param BarLikeRepository $barRepo
     * @return Response
     */
    public function like(ObjectManager $manager, Bar $bar, BarLikeRepository $barRepo): Response{
        $user= $this->getUser();
        if(!$user){
          //  return $this->json(['code'=> 403, 'message'=>'You have to be connected'],403);
            return $this->redirectToRoute('bar.show');
        }
        if ($bar->isLikedByUser($user)){
            $like = $barRepo->findOneBy(['bar'=>$bar, 'user'=>$user]);
            $manager->remove($like);
            $manager->flush();
            return $this->redirectToRoute('bar.show');
        }
        $like = new BarLike();
        $like->setUser($user);
        $like ->setBar($bar);
        $manager->persist($like);
        $manager->flush();
        return $this->redirectToRoute('bar.show');
    }



    // ------------------------ Partie commentaire -----------------------------------
    // Ajouter un commentaire
    /**
     * @Route("/showBar/commentaire", name="commentaire.add")
     */
    public function addCommentaire(Environment $twig, RegistryInterface $doctrine, Request $request){
     //   $donnees['contenue']=$request->get('contenue');
        $donnees['id']= $request->get('bar');
        $bar= $doctrine->getRepository(Bar::class)->findOneBy(['id'=>$request->get('bar')]);
        $user=$this->getUser();
        $commentaire = new Commentaire();
        $commentaire->setUser($user)
            ->setBar($bar)
            ->setContenue($request->get('contenue'));
        $doctrine->getEntityManager()->persist($commentaire);
        $doctrine->getEntityManager()->flush();
        return $this->redirectToRoute('bar.show');
    }

    // Supprimer un commentaire
    /**
     * @Route("/showBar/commentaire/del", name="commentaire.del")
     */
    public function deleteCommentaire(Request $request, Environment $twig, RegistryInterface $doctrine, FormFactoryInterface $formFactory)
    {
        $commentaire=$doctrine->getRepository(Commentaire::class)->find($request->query->get('id'));
        $doctrine->getEntityManager()->remove($commentaire);
        $doctrine->getEntityManager()->flush();
        return $this->redirectToRoute('bar.show');
    }






    // ------------------------------ Partie Admin -------------------------------------
    // affichage de la liste des bars
    /**
     * @Route("/admin/showBar", name="admin.showBar")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showBarAdmin(RegistryInterface $doctrine,Environment $twig){
        $bar = $doctrine->getRepository(Bar::class)->findAll();
        return new Response($twig->render('admin/bar/showBar.html.twig', ['bar' => $bar]));
    }

    // supprimer un bar par l'admin
    /**
     * @Route("/admin/delete/bar", name="admin.deleteBar", methods={"DELETE"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteUserAdmin(Request $request, Environment $twig, RegistryInterface $doctrine)
    {
        $bar=$doctrine->getRepository(Bar::class)->find($request->query->get('id'));
        $doctrine->getEntityManager()->remove($bar);
        $doctrine->getEntityManager()->flush();
        return $this->redirectToRoute('admin.showBar');
    }

    // ajouter un bar par l'admin
    /**
     * @Route("/admin/addBar", name="admin.addBar", methods={"POST"})
     */
    public function ajouterBarAdmin(Request $request, Environment $twig, RegistryInterface $doctrine){
        $bar = new Bar();
        $form=$this->createForm(BarType::class, $bar);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $bar=$form->getData();
            $doctrine->getEntityManager()->persist($bar);
            $doctrine->getEntityManager()->flush();

            return $this->redirectToRoute('admin.showBar');
        }
        return $this->render('admin/bar/inscrireBar.html.twig', [
            'form' => $form->createView()
        ]);
    }

    // modifier un bar
    /**
     * @Route("/admin/editBar", name="admin.editBar", methods={"PUT"})
     */
    public function editBarAdmin(Request $request, RegistryInterface $doctrine, FormFactoryInterface $formFactory,UserPasswordEncoderInterface $encoder)
    {
        $bar=$doctrine->getRepository(Bar::class)->find($request->query->get('id'));
        $form=$formFactory->createBuilder(ModifBarType::class,$bar,  array(
            'method' => 'PUT',
        ))->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $doctrine->getEntityManager()->flush();
            return $this->redirectToRoute('admin.showBar');
        }
        return $this->render('admin/bar/modifierBar.html.twig',['form'=>$form->createView()]);
    }
}
