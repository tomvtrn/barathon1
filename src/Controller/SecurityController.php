<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class SecurityController extends Controller
{


    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authUtils)
    {
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return $this->render('security/connexion.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    /**
     * @Route("/registration", name="user_registration",methods={"GET","POST"})
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder, AuthenticationUtils $authUtils)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        $user->setRoles('ROLE_USER');
        if ($form->isSubmitted() && $form->isValid()) {
            $recaptcha = new \ReCaptcha\ReCaptcha('6Ldd4cEUAAAAADAGPTPXHdUszai86Z5pwpFuXtFM');
            $resp = $recaptcha->setExpectedHostname('127.0.0.1')
                ->verify($_POST['g-recaptcha-response']);
            if (isset($_POST['g-recaptcha-response'])) {
                if ($resp->isSuccess()) {
                    $hash = $encoder->encodePassword($user, $user->getPassword());
                    $user->setPassword($hash);
                    $manager->persist($user);
                    $manager->flush();
                    return $this->redirectToRoute('login');
                } else {

                    return $this->render('security/inscription.html.twig', [
                        'form' => $form->createView()
                    ]);
                }
            }
        }
        return $this->render('security/inscription.html.twig', [
            'form' => $form->createView()
        ]);
    }

}
