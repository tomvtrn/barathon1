<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\User;
use App\Form\ContactType;
use App\Notification\ContactNotification;
use App\Services\Mailer;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Twig\Environment;
use Symfony\Component\Form\FormTypeInterface;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index.index")
     */
    public function index(Request $request, Environment $twig)
    {

//        if(! is_null($this->getUser())){
//            echo "<br>";
//            echo " id: ".$this->getUser()->getId();
//            echo " roles :   ";
//            print_r($this->getUser()->getRoles());
//            die();
//        }

        if($this->isGranted('ROLE_ADMIN')) {
            //return $this->redirectToRoute('admin.index');
            return new Response($twig->render('accueil.html.twig'));
        }
        if($this->isGranted('ROLE_CLIENT')) {
            return new Response($twig->render('accueil.html.twig'));
            //return new Response($twig->render('frontOff/frontOFFICE.html.twig'));
        }
        return new Response($twig->render('accueil.html.twig'));

    }

    /**
     * @Route("/client", name="index.client")
     */
    public function indexClient(Request $request, Environment $twig)
    {
        if($this->isGranted('ROLE_ADMIN')) {
            //return $this->redirectToRoute('admin.index');
            return new Response($twig->render('accueil.html.twig'));
        }
        if($this->isGranted('ROLE_USER')) {
            // return $this->redirectToRoute('client.index');
            return new Response($twig->render('accueil.html.twig'));
        }
        return new Response($twig->render('accueil.html.twig'));

    }




    /**
     * @Route("/propos", name="propos")
     */
    public function aPropos(Request $request, Mailer $mailer, RegistryInterface $doctrine)
    {
        // création d'un formulaire "à la volée", afin que l'internaute puisse renseigner son mail
        $contact = new Contact();
        $form= $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $contact=$form->getData();
            $doctrine->getEntityManager()->persist($contact);
            $doctrine->getEntityManager()->flush();


            // on utilise le service Mailer créé précédemment
            $bodyMail = $mailer->createBodyMail('contact/email.html.twig', ['contact'=>$contact]);

            $mailer->sendMessage('projetweb.belfort@zary.fr', 'tomvautr1@gmail.com', 'Requete', $bodyMail);
            $request->getSession()->getFlashBag()->add('success', "Le mail à bien été envoyé à l'admin");

            return $this->render('contact/apropos.html.twig',[
                'form' => $form->createView()
            ]);
        }

        return $this->render('contact/apropos.html.twig',[
        'form' => $form->createView()
        ]);
    }

}
