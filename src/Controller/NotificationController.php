<?php

namespace App\Controller;

use App\Controller\UserController;
use App\Controller\BarController;
use App\Entity\Bar;
use App\Entity\Evenement;
use App\Repository\UserRepository;
use App\Entity\Amis;
use App\Entity\User;
use App\Entity\Notification;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Tests\B;
use Twig\Environment;

class NotificationController extends AbstractController
{
    /**
     * @Route("/notification", name="notification")
     */
    public function index()
    {
        return $this->render('notification/index.html.twig', [
            'controller_name' => 'NotificationController',
        ]);
    }

    // affiche les notifications
    public function showNotifications(Request $request, Environment $twig, RegistryInterface $doctrine)
    {
        $user = $this->getUser();
        $notifications = $doctrine->getRepository(Notification::class)->findBy(["users" => $user]);

        return $this->render('notification/_showNotifications.html.twig', [
            'notifications' => $notifications
        ]);
    }

    // faire une requete pour demander un utilisateur en amis (creer une notification)
    /**
     * @Route("/userAmis/list", name="user.addAmis")
     */
    public function demandeAmis(Request $request, Environment $twig,RegistryInterface $doctrine){
        $user=$this->getUser();
        $use=$doctrine->getRepository(User::class)->find($request->query->get('id'));
        $amis= new Amis();
        $amis->setUserId1($use);
        $amis->setUserId2($user);
        $amis->setIsAmis(0);
        $doctrine->getEntityManager()->persist($amis);

        $notif= new Notification();
        $notif->setDemandeAmis($amis);
        $notif->setUsers($use);

        $doctrine->getEntityManager()->persist($notif);
        $doctrine->getEntityManager()->flush();
        $userControl=$doctrine->getRepository(User::class)->findByRole('ROLE_USER');

        $amiss = $doctrine->getRepository(Amis::class)->getAmis($user);




        return new Response($twig->render('user/listeUser.html.twig', ['user' => $userControl, 'amis' => $amiss]));
    }

    // accepte la demande d'amis
    /**
     * @Route("/userAmis/list/accepte",name="amis.accepte")
     */
    public function accepteAmis(Request $request, Environment $twig, RegistryInterface $doctrine){
        $notif=$doctrine->getRepository(Notification::class)->find($request->query->get('id'));
        $notif->getDemandeAmis()->setIsAmis(1);
        $doctrine->getEntityManager()->persist($notif);
        $doctrine->getEntityManager()->flush();
        $doctrine->getEntityManager()->remove($notif);
        $doctrine->getEntityManager()->flush();

        $bar=$doctrine->getRepository(Bar::class)->findAll();
        return $this->redirectToRoute('user.userList');

    }

    // refuse la demande d'amis
    /**
     * @Route("/userAmis/list/acceptePas", name="amis.acceptePas")
     */
    public function acceptePasAmis(Request $request, Environment $twig, RegistryInterface $doctrine){
        $notif=$doctrine->getRepository(Notification::class)->find($request->query->get('id'));
        $doctrine->getEntityManager()->remove($notif);
        $doctrine->getEntityManager()->flush();
        //$userControl=$doctrine->getRepository(User::class)->findByRole('ROLE_USER');
        $bar=$doctrine->getRepository(Bar::class)->findAll();
        return $this->redirectToRoute('user.userList');
    }

    // page evenement

    /**
     * @Route("/show/evenement", name="evenement.show")
     */
    public function showEvenement(Request $request, Environment $twig, RegistryInterface $doctrine){
        $user= $doctrine->getRepository(User::class)->findByRole('ROLE_USER');
        $bar=$doctrine->getRepository(Bar::class)->findAll();

        return new Response($twig->render('notification/evenement.html.twig', ['bar' => $bar, 'user' => $user]));
    }


    // Ajouter un événement
    /**
     * @Route("/add/evenement", name="evenement.add", methods={"POST"})
     */
    public function addEvenement(Request $request, Environment $twig, RegistryInterface $doctrine){
        $evenement = new Evenement();
        $use = $this->getUser();
        $bar= $doctrine->getRepository(Bar::class)->find($request->get('bar_id'));
        foreach ($_POST['amis_id'] as $invite){
            $user= $doctrine->getRepository(User::class)->find($invite);
            $evenement->setBar($bar)
                ->setInvite($user)
                ->setDate($request->request->get('madate'))
                ->setNom($request->request->get('nomEvent'));
            $doctrine->getEntityManager()->persist($evenement);

            $notif = new Notification();
            $notif->setEvenement($evenement)
                ->setUsers($user);
            $doctrine->getEntityManager()->persist($notif);

        }


        $notif = new Notification();
        $notif->setEvenement($evenement)
            ->setUsers($use);

        $doctrine->getEntityManager()->persist($notif);
        $doctrine->getEntityManager()->flush();
        return $this->redirectToRoute('evenement.show');
    }
}
