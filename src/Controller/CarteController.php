<?php

namespace App\Controller;

use App\Entity\Bar;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class CarteController extends AbstractController
{
    /**
     * @Route("/carte", name="carte")
     */
    public function index()
    {
        return $this->render('carte/index.html.twig', [
            'controller_name' => 'CarteController',
        ]);
    }

    public function getBars(Request $request, Environment $twig, RegistryInterface $doctrine)
    {
        $bars = $doctrine->getRepository(Bar::class)->findBy([],['id'=>'ASC']);

        return $this->render('carte/_showMap.html.twig', [
            'bars' => $bars
        ]);
    }
}
