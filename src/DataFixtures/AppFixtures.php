<?php
namespace App\DataFixtures;

use App\Entity\Amis;
use App\Entity\Bar;
use App\Entity\Notification;
use App\Entity\Message;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\User;


class AppFixtures extends Fixture
{
    // ...https://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    public function load(ObjectManager $manager)
    {
        $this->loadBar($manager);
        $this->loadUser($manager);
        $this->loadAmis($manager);
        $this->loadNotifications($manager);
    }


    // fin TD
    public function loadBar(objectManager $manager){
        // les types de evenements
        $differentBar = [
            ['photo'=>'lion_flandres.jpg','Email' => "", 'Latitude' => '47,635432', 'Nom' => 'Le lion des Flandres' , 'Telephone' => '0662786269' , 'Ville' => 'Belfort' , 'Adresse' => '70 Faubourg de France' , 'Code_postal' => '90000' , 'Description' => 'Venez découvrir toute sorte de bières belges et du Nord toutes à 4 euros, de la trappiste à la bière artisanale 5 bières pression. Espace en terrasse lors des beaux jours et espace détente a l interieur du bar. ' , 'Longitude' => '6.853545'],

            ['photo'=>'abc.jpg','Email' => "", 'Latitude' => '47,635237', 'Nom' => "Bar l'ABC" , 'Telephone' => '0384283551' , 'Ville' => 'Belfort' , 'Adresse' => '72 Faubourg de France' , 'Code_postal' => '90000' , 'Description' => 'Ce bar-brasserie-restaurant est situé pres de la gare et vous accueille toute la journée. On peut y déguster des coctails ainsi que des plats fait maison dans un cadre chaleureux et convivial.', 'Longitude' => '6.853416'],

            ['photo'=>'oneShot.jpg','Email' => "", 'Latitude' => '47,632967', 'Nom' => 'The One Shot' , 'Telephone' => '0384263215' , 'Ville' => 'Belfort' , 'Adresse' => '23 Avenue Wilson' , 'Code_postal' => '90000' , 'Description' => 'Le One Shot vous propose de boire un verre ou manger tout en proposant un cadre chaleulreux, des jeux de sociétés et des jeux vidéo.', 'Longitude' => '6.854767'],

            ['photo'=>'estaminet.JPG','Email' => "", 'Latitude' => '47,638736', 'Nom' => "L'estaminet" , 'Telephone' => '0370923983' , 'Ville' => 'Belfort' , 'Adresse' => '4 rue de la Porte de France' , 'Code_postal' => '90000' , 'Description' => 'Installé dans la plus ancienne épicerie de France, l\'Estaminet propose une large gamme de bières à la pression ainsi qu\'une gamme de vin au verre et à la bouteille. petite restauration flammekueche, saucisson d\'Ardèche dans un cadre atypique.', 'Longitude' => '6.862486'],

            ['photo'=>'aux-trois-maillets.jpg','Email' => "", 'Latitude' => '47,638253', 'Nom' => 'Aux trois maillets' , 'Telephone' => '0384280601' , 'Ville' => 'Belfort' , 'Adresse' => "3 Place d'Armes" , 'Code_postal' => '90000' , 'Description' => 'Restaurant Bar Brasserie Glacier situé à l\'entrée de la vieille ville, à Belfort. Cuisine traditionnelle, coupes glacées, ..terrasse ensolleillée.', 'Longitude' => '6.863480'],

            ['photo'=>'la-voile-sucree.jpg','Email' => "", 'Latitude' => '47,640761', 'Nom' => 'La voile sucrée' , 'Telephone' => '0384367486' , 'Ville' => 'Belfort' , 'Adresse' => '2 Bis Rue Georges Clemenceau' , 'Code_postal' => '90000' , 'Description' => 'Installée dans une ancienne brasserie du 19ème siècle, la Voile sucrée est un lieu cosy, parfait pour se retrouver entre amis.', 'Longitude' => '6.859536'],

            ['photo'=>'Bistrot_des_moines.jpeg','Email' => "", 'Latitude' => '47,638448', 'Nom' => 'Bistrot des moines' , 'Telephone' => '0384218640' , 'Ville' => 'Belfort' , 'Adresse' => '22 Rue P Dreyfus Schmidt' , 'Code_postal' => '90000' , 'Description' => 'Vous désirez manger une cuisine traditionnelle ? Vous voulez déguster une bonne bière ? Le Bistrot des Moines est la pour ça. Situé au centre-ville et ouvert du lundi au samedi.', 'Longitude' => '6.859429'],

            ['photo'=>'bar-le-clan.jpeg','Email' => "", 'Latitude' => '47,633011', 'Nom' => 'Bar Le Clan' , 'Telephone' => '0981131602' , 'Ville' => 'Belfort' , 'Adresse' => '4 Rue Aristide Briand' , 'Code_postal' => '90000' , 'Description' => 'Le Clan est situé dans un endroit tranquille du centre ville. Ce Bar Brasserie vous propose de gouter a ses plats ainsi que d\'assister a ses soirées a thèmes dans une ambiance conviviale.', 'Longitude' => '6.856580'],

            ['photo'=>'Le_Bar_Atteint.jpg','Email' => "", 'Latitude' => '47,649042', 'Nom' => 'Le Bar Atteint' , 'Telephone' => '0983918295' , 'Ville' => 'Belfort' , 'Adresse' => '25 rue de la Savoureuse' , 'Code_postal' => '90000' , 'Description' => 'Lieu chaleureux, participatif et très inspiré de l’éducation populaire. Nous vous proposons des repas le midi à base de produits locaux ou bio et le soir des événements citoyens et culturels.', 'Longitude' => '6.854913'],

            ['photo'=>'Le_Finnegans.jpeg','Email' => "", 'Latitude' => '47,638164', 'Nom' => "Le Finnegan's" , 'Telephone' => '0384282028' , 'Ville' => 'Belfort' , 'Adresse' => '6 Boulevard Carnot' , 'Code_postal' => '90000' , 'Description' => 'Authentique pub irlandais, venez déguster une bonne bière en pression au Finnegans. Le bar vous propose une large gamme de bières pression le tout, dans une authentique ambiance irlandaise.', 'Longitude' => '6.859696'],

            ['photo'=>'Le_Bar_Atteint.jpg','Email' => "", 'Latitude' => '47.6276878', 'Nom' => 'Bar Restaurant du Château d’Eau' , 'Telephone' => '0983918295' , 'Ville' => 'Belfort' , 'Adresse' => '14 Rue de Bavilliers' , 'Code_postal' => '90000' , 'Description' => 'Lieu chaleureux, participatif et très inspiré de l’éducation populaire. Nous vous proposons des repas le midi à base de produits locaux ou bio et le soir des événements citoyens et culturels.', 'Longitude' => '6.8531855'],

            ['photo'=>'Le_Bar_Atteint.jpg','Email' => "", 'Latitude' => '47.6405825', 'Nom' => 'Igloo\' Bar' , 'Telephone' => '0983918295' , 'Ville' => 'Belfort' , 'Adresse' => '14 Avenue du Château d\'Eau, 90000 Belfort' , 'Code_postal' => '90000' , 'Description' => 'Lieu chaleureux, participatif et très inspiré de l’éducation populaire. Nous vous proposons des repas le midi à base de produits locaux ou bio et le soir des événements citoyens et culturels.', 'Longitude' => '6.8376141'],

        ];
        foreach ($differentBar as $bar)
        {
            $bar_new = new Bar();
            $bar_new->setPhoto($bar['photo']);
            $bar_new->setEmail($bar['Email']);
            $bar_new->setLatitude($bar['Latitude']);
            $bar_new->setNom($bar['Nom']);
            $bar_new->setTelephone($bar['Telephone']);
            $bar_new->setVille($bar['Ville']);
            $bar_new->setAdresse($bar['Adresse']);
            $bar_new->setCodePostal($bar['Code_postal']);
            $bar_new->setDescription($bar['Description']);
            $bar_new->setLongitude($bar['Longitude']);
            $manager->persist($bar_new);
            $manager->flush();
        }
    }


    public function loadUser(ObjectManager $manager){
        $user=[
            ["username"=>"admin","password"=>"admin","nom"=>"admin","prenom"=>"admin","email"=>"tomvautr1@gmail.com","roles"=>"ROLE_ADMIN","directeur"=>0],
            ["username"=>"pdg","password"=>"pdg","nom"=>"pdg","prenom"=>"pdg","email"=>"pdg@exemple.com","roles"=>"ROLE_USER","directeur"=>1],
            ["username"=>"user","password"=>"user","nom"=>"user","prenom"=>"user","email"=>"user@exemple.com","roles"=>"ROLE_USER","directeur"=>0],
            ["username"=>"user2","password"=>"user2","nom"=>"user","prenom"=>"user2","email"=>"user2@exemple.com","roles"=>"ROLE_USER","directeur"=>0],
        ];
        foreach ($user as $use){
            $use_new = new User();
            $password = $this->encoder->encodePassword($use_new, $use['password']);
            $use_new->setPassword($password);
            $use_new->setUsername($use['username']);
            $use_new->setNom($use['nom']);
            $use_new->setPrenom($use['prenom']);
            $use_new->setEmail($use['email']);
            $use_new->setRoles($use['roles']);
            $use_new->setDirecteur($use['directeur']);
            $manager->persist($use_new);
            $manager->flush();

        }
    }

    public function loadAmis(ObjectManager $manager){
        $allUser = $manager->getRepository(User::class);
        $user = $allUser->findOneBy(['username' => 'admin']);
        $use_new = $allUser->findOneBy(['username' => 'user']);
        $user_new = $allUser->findOneBy(['username' => 'user2']);

        $amis = new Amis();
        $amis->setUserId1($user);
        $amis->setUserId2($use_new);
        $amis->setIsAmis(1);
        $manager->persist($amis);

        $amis1 = new Amis();
        $amis1->setUserId1($user_new);
        $amis1->setUserId2($user);
        $amis1->setIsAmis(1);
        $manager->persist($amis1);

        $manager->flush();
    }



    public function loadNotifications(ObjectManager $manager){

        $allUser = $manager->getRepository(User::class);
        $user = $allUser->findOneBy(['username' => 'admin']);
        $use_new = $allUser->findOneBy(['username' => 'user']);
        $user_new = $allUser->findOneBy(['username' => 'user2']);

        $amis= new Amis();
        $amis->setUserId1($use_new);
        $amis->setUserId2($user_new);
        $amis->setIsAmis(0);
        $manager->persist($amis);

        $notif = new Notification();
        $notif->setDemandeAmis($amis)
            ->setUsers($use_new);
        $manager->persist($notif);


        $message= new Message();
        $message->setUser1($user);
        $message->setUser2($use_new);
        $message->setContenue("Salut");
        $manager->persist($message);

        $use_new = $allUser->findOneBy(['username' => 'user2']);

        $message= new Message();
        $message->setUser1($user);
        $message->setUser2($use_new);
        $message->setContenue("wesh Sarko");
        $manager->persist($message);

        $manager->flush();
    }
}
