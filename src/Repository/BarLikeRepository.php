<?php

namespace App\Repository;

use App\Entity\BarLike;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BarLike|null find($id, $lockMode = null, $lockVersion = null)
 * @method BarLike|null findOneBy(array $criteria, array $orderBy = null)
 * @method BarLike[]    findAll()
 * @method BarLike[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BarLikeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BarLike::class);
    }



    // /**
    //  * @return BarLike[] Returns an array of BarLike objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BarLike
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
