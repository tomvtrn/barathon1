<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="cet Email existe déjà")
 * @UniqueEntity(fields="username", message="Nom déjà utilisé")
 */
class User  implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OneToMany(targetEntity="App\Entity\Amis", mappedBy="user_id1")
     * @ORM\OneToMany(targetEntity="App\Entity\Amis", mappedBy="user_id2")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Length(min="6", minMessage="Votre mot de passe doit faire minimum 6 caractères")
     */
    private $password;

    /**
     * @Assert\EqualTo(propertyPath="password", message="Les mots de passe saisis sont différents. Veuillez réessayer." )
     */
    public $confirm_password;


    /**
     * @ORM\Column(type="string", length=60, unique=true )
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $nom;

    /**
     * @ORM\Column(name="Prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(name="Date_de_naissance", type="date", nullable=true)
     */
    private $dateNaissance;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $tokenMail = '';


    /**
     * @ORM\Column(name="roles", type="string", length=64)
     */
    private $roles="ROLE_USER";

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $directeur;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bar", mappedBy="users")
     */
    private $bar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BarLike", mappedBy="user")
     */
    private $likes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notification", mappedBy="users")
     */
    private $notifications;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @Vich\UploadableField(mapping="images", fileNameProperty="photo")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commentaire", mappedBy="user")
     */
    private $commentaires;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="user1")
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="user2")
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenement", mappedBy="invite")
     */
    private $evenements;

    public function setImageFile(File $photo = null)
    {
        $this->imageFile = $photo;
    }
    public function getImageFile()
    {
        return $this->imageFile;
    }


    public function getId(){
        return $this->id;
    }
    public function __construct()
    {
        $this->bar = new ArrayCollection();
        $this->amis = new ArrayCollection();

        $this->likes = new ArrayCollection();


        $this->likes = new ArrayCollection();


        $this->notifications = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->evenements = new ArrayCollection();
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }


    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }


    public function getPassword()
    {
        return $this->password;
    }


    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }


    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }


    public function getEmail()
    {
        return $this->email;
    }


    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * @return string
     */
    public function getTokenMail():string
    {
        return $this->tokenMail;
    }

    /**
     * @param string $tokenMail
     */
    public function setTokenMail($tokenMail):void
    {
        $this->tokenMail = $tokenMail;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getRoles()
    {
        if ($this->roles)
            return [$this->roles];
        else
            return ['ROLE_USER'];
    }

    public function setRoles($roles)
    {
        $this->roles = $roles;
        // allows for chaining
        return $this;
    }

    public function getDirecteur(): ?bool
    {
        return $this->directeur;
    }

    public function setDirecteur(?bool $directeur): self
    {
        $this->directeur = $directeur;

        return $this;
    }

    /**
     * @return Collection|bar[]
     */
    public function getBar(): Collection
    {
        return $this->bar;
    }

    public function addBar(bar $bar): self
    {
        if (!$this->bar->contains($bar)) {
            $this->bar[] = $bar;
            $bar->setUsers($this);
        }

        return $this;
    }

    public function removeBar(bar $bar): self
    {
        if ($this->bar->contains($bar)) {
            $this->bar->removeElement($bar);
            // set the owning side to null (unless already changed)
            if ($bar->getUsers() === $this) {
                $bar->setUsers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Amis[]
     */
    public function getAmis(): Collection
    {
        return $this->amis;
    }

    public function addAmi(Amis $ami): self
    {
        if (!$this->amis->contains($ami)) {
            $this->amis[] = $ami;
            $ami->setUserId1($this);
        }

        return $this;
    }

    public function removeAmi(Amis $ami): self
    {
        if ($this->amis->contains($ami)) {
            $this->amis->removeElement($ami);
            // set the owning side to null (unless already changed)
            if ($ami->getUserId1() === $this) {
                $ami->setUserId1(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BarLike[]
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(BarLike $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
            $like->setUser($this);
        }

        return $this;
    }


    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setUsers($this);

        }

        return $this;
    }




    public function removeLike(BarLike $like): self
    {

        if ($this->likes->contains($like)) {
            $this->likes->removeElement($like);
            // set the owning side to null (unless already changed)
            if ($like->getUser() === $this) {

                $like->setUser(null);
            }
        }
        return $this;


    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->contains($notification)) {
            $this->notifications->removeElement($notification);
            // set the owning side to null (unless already changed)
            if ($notification->getUsers() === $this) {
                $notification->setUsers(null);

            }
        }

        return $this;
    }
    
    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }


    /**
     * @return Collection|Commentaire[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setUser($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->contains($commentaire)) {
            $this->commentaires->removeElement($commentaire);
            // set the owning side to null (unless already changed)
            if ($commentaire->getUser() === $this) {
                $commentaire->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setUser1($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->contains($message)) {
            $this->messages->removeElement($message);
            // set the owning side to null (unless already changed)
            if ($message->getUser1() === $this) {
                $message->setUser1(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Evenement[]
     */
    public function getEvenements(): Collection
    {
        return $this->evenements;
    }

    public function addEvenement(Evenement $evenement): self
    {
        if (!$this->evenements->contains($evenement)) {
            $this->evenements[] = $evenement;
            $evenement->setInvite($this);
        }

        return $this;
    }

    public function removeEvenement(Evenement $evenement): self
    {
        if ($this->evenements->contains($evenement)) {
            $this->evenements->removeElement($evenement);
            // set the owning side to null (unless already changed)
            if ($evenement->getInvite() === $this) {
                $evenement->setInvite(null);
            }
        }

        return $this;
    }
}