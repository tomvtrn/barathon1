<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AmisRepository")
 */
class Amis
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAmis;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user_id1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user_id2;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notification", mappedBy="demandeAmis")
     */
    private $notifications;

    public function __construct()
    {
        $this->notifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsAmis(): ?bool
    {
        return $this->isAmis;
    }

    public function setIsAmis(?bool $isAmis): self
    {
        $this->isAmis = $isAmis;

        return $this;
    }

    public function getUserId1(): ?User
    {
        return $this->user_id1;
    }

    public function setUserId1(?User $user_id1): self
    {
        $this->user_id1 = $user_id1;

        return $this;
    }

    public function getUserId2(): ?User
    {
        return $this->user_id2;
    }

    public function setUserId2(?User $user_id2): self
    {
        $this->user_id2 = $user_id2;

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setDemandeAmis($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->contains($notification)) {
            $this->notifications->removeElement($notification);
            // set the owning side to null (unless already changed)
            if ($notification->getDemandeAmis() === $this) {
                $notification->setDemandeAmis(null);
            }
        }

        return $this;
    }
}
