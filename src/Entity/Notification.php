<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Amis", inversedBy="notifications")
     */
    private $demandeAmis;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="notifications")
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Evenement", inversedBy="notifications")
     */
    private $evenement;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDemandeAmis(): ?Amis
    {
        return $this->demandeAmis;
    }

    public function setDemandeAmis(?Amis $demandeAmis): self
    {
        $this->demandeAmis = $demandeAmis;

        return $this;
    }


    public function getUsers(): ?User
    {
        return $this->users;
    }

    public function setUsers(?User $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getEvenement(): ?Evenement
    {
        return $this->evenement;
    }

    public function setEvenement(?Evenement $evenement): self
    {
        $this->evenement = $evenement;

        return $this;
    }

}
